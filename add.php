<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="styles.css">
	<title>Product Add</title>
	
	<?php
		include_once './includes/autoloader.php';
		include_once './includes/functions.php';
	?>

</head>
<body>
	<div class="mainPageDiv">    
		<div class="topBarDiv">

			<h1 class="h1">Product Add</h1>

			<div class="topButtonsDiv">
				<button type="submit" form="mainForm" class="topButton">Save</button>
				<a href="list.php"><button class="topButton">Cancel</button></a>
			</div>

		</div>
		<hr>
		<div class="productAddMainDiv">
			<form id="mainForm" class="productAddForm" action="./includes/productadder.php" method="POST">

				<p class="errorFont"> 
					<?php  
						$fullUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
						if (strpos($fullUrl, "product=alreadyInDb") == true) {
							echo "SKU needs to be unique to each item";
						}
					?>
				</p>
				
				<label for="sku">SKU</label>
				<input id="sku" name="sku" type="text" pattern="[a-zA-Z0-9]{5,10}" placeholder="XYZ4567890" required
					oninvalid="this.setCustomValidity('Enter a unique 5-10 character SKU')"
					oninput="this.setCustomValidity('')">

				<label for="productname">Name</label>
				<input id="productname" name="productname" type="text" required 
					oninvalid="this.setCustomValidity('Product name is required')"
					oninput="this.setCustomValidity('')"><br>

				<label for="price">Price ($)</label>
				<input id="price" name="price" type="text" pattern="\d+(\.\d{2})?" required 
					oninvalid="this.setCustomValidity('Enter full price. Eg. 12 or 5.99')"
					oninput="this.setCustomValidity('')"><br>
				<label class="labelWide" for="productType">Type Switcher</label>

				<select name="productType" id="productType" onchange="changeFunction()">
					<option value="dvd">DVD</option>
					<option value="book">Book</option>
					<option value="furniture">Furniture</option>
				</select><br>

				<div id="dvd">
					<label class="labelWide" for="prdsize">Size (MB)</label>
					<input class="textInput" id="prdsize" name="prdsize" type="number" required
					oninvalid="this.setCustomValidity('Size is required')"
					oninput="this.setCustomValidity('')"><br><br><br>
					<p class="descriptionFont">Please, provide the size of the DVD</p>
				</div>

				<div id="book" hidden>
					<label class="labelWide" for="prdweight">Weight (KG)</label>
					<input class="textInput" id="prdweight" name="prdweight" type="number" 
					oninvalid="this.setCustomValidity('Weight is required')"
					oninput="this.setCustomValidity('')"><br><br><br>
					<p class="descriptionFont">Please, provide the weight of the book</p>
				</div>

				<div id="furniture" hidden>
					<label class="labelWide" for="prdheight">Height (CM)</label>
					<input class="textInput" id="prdheight" name="prdheight" type="number" 
					oninvalid="this.setCustomValidity('Height is required')"
					oninput="this.setCustomValidity('')"><br>
					<label class="labelWide" for="prdwidth">Width (CM)</label>
					<input class="textInput" id="prdwidth" name="prdwidth" type="number" 
					oninvalid="this.setCustomValidity('Width is required')"
					oninput="this.setCustomValidity('')"><br>
					<label class="labelWide" for="prdlength">Length (CM)</label>
					<input class="textInput" id="prdlength" name="prdlength" type="number" 
					oninvalid="this.setCustomValidity('Length is required')"
					oninput="this.setCustomValidity('')"><br><br><br><br><br><br>
					<p class="descriptionFont" >Please, provide the dimensions of the furniture</p>
				</div>

			</form>

			<?php echo "<script>changeFunction();</script>";?>
			</div>
		</div>
		<hr>
		<p class="centeredText">Robert Harmaakivi - Scandiweb Test assignment</p>
	</div>
</body>
</html>