<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Product List</title>
    <?php
        include_once './includes/autoloader.php';
    ?>
</head>
<body>
    <div class="mainPageDiv">    
        <div class="topBarDiv">
            <h1 class="h1">Product list</h1>
            <div class="topButtonsDiv">
                <a href="add.php"><button class="topButton">ADD</button></a>
                <button type="submit" form="massDeleteForm" class="topButton">MASS DELETE</button>
            </div>
        </div>
        <hr>

        <div class="mainContentDiv">
            <form id="massDeleteForm" class="mainContentDiv" action="./includes/massdelete.php" method="post">

                <?php
                    $productHandler = new ProductHandler();
                    $productHandler->printProductsFromDb();
                ?>

            </form>
        </div>
        <hr>
        <p class="centeredText"> Robert Harmaakivi - Scandiweb Test assignment</p>
    </div>
</body>
</html>