<?php

// Has all the database handling functionality

class ProductHandler extends Dbh{

    public function printProductsFromDb(){

        $sql = "SHOW TABLES";
        $stmt = $this->connect()->query($sql);
        while ($table = $stmt->fetch(PDO::FETCH_NUM)) {
            $sqlSelect = "SELECT * FROM $table[0];";
            $stmtSelect = $this->connect()->query($sqlSelect);
            while ($row = $stmtSelect->fetch(PDO::FETCH_NUM)) {
                $table[0]::PrintProduct($row);
            }
        }
    }


    public function addProductToDb($product){

        $productVarsArray = array();
        $productVars = "";
        $questionMarks = "";
        foreach ($product as $key => $value) {
            if ($key != "productType") {
                $productVarsArray[] = $value;
                $productVars .= $key . ", ";
                $questionMarks .= "?, ";
            }
        }

        $productVars = substr($productVars, 0, -2);
        $questionMarks = substr($questionMarks, 0, -2);

        $sql = "INSERT INTO {$product->productType} ($productVars) VALUES ($questionMarks)";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute($productVarsArray);
    }


    public function deleteFromDb($sku){

        $sql = "SHOW TABLES";
        $stmt = $this->connect()->query($sql);
        while ($table = $stmt->fetch(PDO::FETCH_NUM)) {
            $sqlSelect = "SELECT * FROM $table[0];";
            $stmtSelect = $this->connect()->query($sqlSelect);
            while ($row = $stmtSelect->fetch(PDO::FETCH_NUM)) {
                if ($row[0] == $sku) {
                    $sqlDelete = "DELETE FROM $table[0] WHERE sku='$sku'";
                    $stmtDelete = $this->connect()->query($sqlDelete);
                }
            }
        }
    }

    public function checkUniqueSku($sku){

        $sql = "SHOW TABLES";
        $stmt = $this->connect()->query($sql);
        while ($table = $stmt->fetch(PDO::FETCH_NUM)) {
            $sqlSelect = "SELECT * FROM $table[0];";
            $stmtSelect = $this->connect()->query($sqlSelect);
            while ($row = $stmtSelect->fetch(PDO::FETCH_NUM)) {
                if ($row[0] == $sku) {
                header("location: ../add.php?product=alreadyInDb", TRUE);
                exit();
                }
            }
        }
    }
}


?>