<?php

class Dvd extends Product {
    
    //properties
    private $prdsize;

    //getters and setters
    function SetSize($prdsize) {
        $this->prdsize = $prdsize;
    }
    function GetSize() {
        return $this->prdsize;
    }

    //print function

    public static function PrintProduct($row){
        echo 
        '<div class="productDiv">',
            '<input type="checkbox" class="checkmark" name="checkedDeletion[]" value="'.$row[0].'" >',
            '<p class="productCardTopText">',$row[0],'</p>', 
            '<p class="centeredText">',$row[1],'</p>', 
            '<p class="centeredText">',$row[2],' $</p>', 
            '<p class="centeredText"> Size: ',$row[3],' MB</p>',
        '</div>';
    }

}

?>