<?php

class Furniture extends Product {
    
    //properties
    private $prdheight;
    private $prdwidth;
    private $prdlength;

    //getters and setters
    function SetHeight($prdheight) {
        $this->prdheight = $prdheight;
    }
    function GetHeight() {
        return $prdheight->prdheight;
    }
    function SetWidth($prdwidth) {
        $this->prdwidth = $prdwidth;
    }
    function GetWidth() {
        return $prdwidth->prdwidth;
    }
    function SetLength($prdlength) {
        $this->prdlength = $prdlength;
    }
    function GetLength() {
        return $prdlength->prdlength;
    }

    //print function

    public static function PrintProduct($row){
        echo 
        '<div class="productDiv">',
            '<input type="checkbox" class="checkmark" name="checkedDeletion[]" value="'.$row[0].'" >',
            '<p class="productCardTopText">'.$row[0],'</p>', 
            '<p class="centeredText">',$row[1],'</p>', 
            '<p class="centeredText">',$row[2],' $</p>',
            '<p class="centeredText"> Dimensions: ',$row[3],'x',$row[4],'x',$row[5],'</p>',
        '</div>';
    }

}

?>