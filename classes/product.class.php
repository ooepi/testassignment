<?php

abstract class Product {

    //properties
    private $productType;
    private $sku;
    private $productname;
    private $price;

    //getters and setters

    function SetType($productType) {
        $this->productType = $productType;
    }
    function GetType() {
        return $this->productType;
    }
    function SetSKU($sku) {
        $this->sku = $sku;
    }
    function GetSKU() {
        return $this->sku;
    }
    function SetName($productname) {
        $this->productname = $productname;
    }
    function GetName() {
        return $this->productname;
    }
    function SetPrice($price) {
        $this->price = $price;
    }
    function GetPrice() {
        return $this->price;
    }

}

?>