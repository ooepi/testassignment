<?php

    // Product add form points here, to send the information to database

    include 'autoloader.php';

    $productType = $_POST['productType'];

    $product = new $productType();

    foreach ($_POST as $key => $value) {
        if($value){
            $product->$key = $value;
        }
    }

    $productHandler = new ProductHandler();
    $productHandler->checkUniqueSku($_POST['sku']);
    $productHandler->addProductToDb($product);

header("location: ../list.php");
?>
