<?php

// Function to change the form fields visibility, and required status according to the selection

echo "
<script>
function changeFunction() {

    var option = document.getElementById('productType').value;
    var dvdBlock = document.getElementById('dvd');
    var bookBlock = document.getElementById('book');
    var furnitureBlock = document.getElementById('furniture');
    var sizeField1 = document.getElementById('prdsize');
    var weightField1 = document.getElementById('prdweight');
    var heightField1 = document.getElementById('prdheight');
    var widthField1 = document.getElementById('prdwidth');
    var lengthField1 = document.getElementById('prdlength');


    switch(option){
        case 'dvd':
            dvdBlock.hidden = false;
            furnitureBlock.hidden = true;
            bookBlock.hidden = true;
            
            sizeField1.required = true;
            weightField1.required = false;
            heightField1.required = false;
            widthField1.required = false;
            lengthField1.required = false;

            weightField1.value = null;
            heightField1.value = null;
            widthField1.value = null;
            lengthField1.value = null;

            break;
        case 'book':
            dvdBlock.hidden = true;
            furnitureBlock.hidden = true;
            bookBlock.hidden = false;
            
            sizeField1.required = false;
            heightField1.required = false;
            widthField1.required = false;
            lengthField1.required = false;
            weightField1.required = true;

            sizeField1.value = null;
            heightField1.value = null;
            widthField1.value = null;
            lengthField1.value = null;

            break;
        case 'furniture':
            furnitureBlock.hidden = false;
            dvdBlock.hidden = true;
            bookBlock.hidden = true;
            
            sizeField1.required = false;
            weightField1.required = false;
            heightField1.required = true;
            widthField1.required = true;
            lengthField1.required = true;

            sizeField1.value = null;
            weightField1.value = null;

            break;
        default:
            break;
    }
}
</script>
";
?>